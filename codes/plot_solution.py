
from matplotlib import pyplot as plt
from matplotlib import cm               # colormaps
import numpy as np

try:
    fname = 'Ra-gamma.dat'
    x1, y1 = np.loadtxt(fname, unpack=True)
except:
    err_msg = "Could not load data from file %s." % fname \
              + " Did you forget to run the program?"
    raise Exception(err_msg)

try:
    fname = 'kr-gamma.dat'
    x2, y2 = np.loadtxt(fname, unpack=True)
except:
    err_msg = "Could not load data from file %s." % fname \
              + " Did you forget to run the program?"
    raise Exception(err_msg)

try:
    fname = 'wr-gamma.dat'
    x3, y3 = np.loadtxt(fname, unpack=True)
except:
    err_msg = "Could not load data from file %s." % fname \
              + " Did you forget to run the program?"
    raise Exception(err_msg)

font = {'family': 'serif',
        'color':  'black',
        'weight': 'normal',
        'size': 18,
        }


# Solution 1 is plotted: Ra x gamma ###############################################################

fig, ax = plt.subplots()
plt.plot(x1, y1, 'o--',markersize=15)

plt.figure(1)                  

deltax = 1
deltay = 100

plt.axis([x1.min() - deltax, x1.max() + deltax, y1.min() - deltay, y1.max() + deltay])

plt.xlabel(r'$\gamma/\gamma_{0}$', fontdict=font)
plt.ylabel(r'$Ra_{c}$', fontdict=font)

plt.grid(False)

ticklines = ax.get_xticklines() + ax.get_yticklines()
gridlines = ax.get_xgridlines() + ax.get_ygridlines()
ticklabels = ax.get_xticklabels() + ax.get_yticklabels()

for line in ticklines:
    line.set_linewidth(3)

for line in gridlines:
    line.set_linestyle('-.')

for label in ticklabels:
    label.set_color('k')
    label.set_fontsize('16')

plt.title('Critical Rayleigh number', fontdict=font)

plt.savefig('Ra-gamma.pdf')
print 'Saved graphic plot as Ra-gamma.pdf'


# Solution 2 and 3 are plotted: kr/wr x gamma #####################################################

fig, ax = plt.subplots()
plot1, =plt.plot(x2,y2,'bD--', label=r'$k_{c}$', markersize=15)
plot2, =plt.plot(x3,y3, 'ko--', label=r'$\omega_{c}$', markersize=15)
plt.figure(2)

legend = plt.legend(loc = 'lower right', shadow = True, fancybox=True, prop={'size':font['size']})
legend.get_frame().set_facecolor('lightgray')

plt.xlabel(r'$\gamma/\gamma_{0}$', fontdict=font)
plt.ylabel('', fontdict=font)

xmin = min(x3.min(),x2.min())
xmax = max(x3.max(),x2.max())

ymin = min(y3.min(),y2.min())
ymax = max(y3.max(),y2.max())

plt.axis([xmin-1, xmax+1, ymin-1, ymax+1])

ticklines = ax.get_xticklines() + ax.get_yticklines()
gridlines = ax.get_xgridlines() + ax.get_ygridlines()
ticklabels = ax.get_xticklabels() + ax.get_yticklabels()

for line in ticklines:
    line.set_linewidth(3)

for line in gridlines:
    line.set_linestyle('-.')

for label in ticklabels:
    label.set_color('k')
    label.set_fontsize('16')

plt.title('Critical frequency and wave number', fontdict=font)
plt.savefig('krwr-gamma.pdf')
print 'Saved graphic plot as krwr-gamma.pdf'

