# README #

Este README (teste) serve para registrar algumas informações pertinentes.

### What is this repository for? ###

* Armazenar os arquivos do paper do ENCIT
* 16th Brazilian Congress of Thermal Sciences and Engineering
* Páginas importantes: (1) Aqui vai para a página do congresso [Encit][]; e (2) aqui temos as [datas][] importantes.

  [encit]: http://eventos.abcm.org.br/encit2016/
  [datas]: http://eventos.abcm.org.br/encit2016/about/important-dates/

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact